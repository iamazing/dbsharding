# dbsharding

#### 介绍
1. ShardingJDBC分库分表案例，数据库表实现垂直拆分和水平拆分，对业务零侵入。
2. MySQL主从搭建，实现数据读写分离
3. 业务：
![业务关系模型](https://gitee.com/iamazing/dbsharding/raw/master/file/Xnip2019-10-17_10-07-02.jpg)


#### 软件架构
1. springboot
2. mybatis
3. mysql主从架构
4. shardingjdbc实现 分库分表 + 读写分离


#### 代码结构
1. sharding-jdbc-simple :简单的配置案例，包括水平分表，水平分库分表，读写分离等配置
2. shopping :模拟真实场景业务进行分库分表测试。
#### 安装教程
1. git clone
2. mvn clean install
3. run test



#### 使用说明
1. shardingjdbc实现数据库分库分表方案
2. 数据库 水平拆分 + 垂直拆分
3. 表 水平拆分 + 垂直拆分
4. mysq主从架构实现读写分离

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
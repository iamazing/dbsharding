package com.inspire.shopping;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.inspire.shopping.mapper")
public class ShoppingBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingBootstrap.class, args);
    }

}

package com.inspire.shopping.mapper;

import com.inspire.shopping.entity.StoreInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator.
 */
public interface StoreInfoMapper {


    /*List<Map> selectStoreInfoByStoreIds(@Param("storeIds") List<Long> storeIds);

    List<Map> selectStoreInfoByCode(@Param("regionCodes") List<String> regionCodes);*/

    int insertStoreInfo(StoreInfo storeInfo);

    public StoreInfo selectStoreInfoById(Long id);

    public List<StoreInfo> selectStoreInfoList(StoreInfo storeInfo);


    public List<StoreInfo> selectStoreInfoWithRegionList(StoreInfo storeInfo);





    public int updateStoreInfo(StoreInfo storeInfo);

    public int deleteStoreInfoById(Long id);



}

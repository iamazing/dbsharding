package com.inspire.shopping;

import com.inspire.shopping.entity.ProductInfo;
import com.inspire.shopping.entity.StoreInfo;
import com.inspire.shopping.mapper.ProductMapper;
import com.inspire.shopping.mapper.StoreInfoMapper;
import com.inspire.shopping.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShoppingBootstrap.class)
public class ProductInfoTest {

    @Autowired
    private ProductService productService;

    //添加商品 -->都在一个分区，所有使用本地事务即可
    @Test
    public void testCreateProduct(){
        for (int i=1;i<10;i++){
            ProductInfo productInfo = new ProductInfo();
            productInfo.setStoreInfoId(1L);//店铺id

            productInfo.setProductName("薯片"+i);//商品名称
            productInfo.setSpec("大号");
            productInfo.setPrice(new BigDecimal(30));
            productInfo.setRegionCode("110100");
            productInfo.setDescript("薯片！！！"+i);//商品描述
            productService.createProduct(productInfo);
        }

    }


    /**
     * 查询所有商品基本信息
     */
    @Test
    public void testQueryProductInfo(){
        ProductInfo productInfo = new ProductInfo();
        List<ProductInfo> list = productService.queryProductList(productInfo);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
    }



    /**
     * 根据ID查询商品基本信息
     */
    @Test
    public void testQueryProductInfoById(){
        ProductInfo productInfo = new ProductInfo();
        productInfo.setProductInfoId(1L);
        List<ProductInfo> list = productService.queryProductList(productInfo);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
    }

    /**
     * 查询某一个店铺里所有商品的详细信息
     */
    @Test
    public void testQueryProductInfoWithStoreAndDescription(){
        ProductInfo productInfo = new ProductInfo();
        productInfo.setStoreInfoId(1L);
        List<ProductInfo> list = productService.queryProductListWithStoreAndDescription(productInfo);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
    }



}

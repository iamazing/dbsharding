package com.inspire.shopping;

import com.inspire.shopping.entity.Region;

import com.inspire.shopping.mapper.RegionMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 * @apiNote region是字典表，每个数据库都有该表
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShoppingBootstrap.class)
public class RegionTest {

    @Autowired
    private RegionMapper regionMapper;

    //添加数据：
    // TODO: 19/10/9 添加地区  ---->向多个库主库添加数据
    @Test
    public void testInsertRegion(){
        Region region = new Region();
        region.setId(6L);
        region.setRegionCode("410300");
        region.setRegionName("信阳市");
        region.setLevel(1);
        region.setParentRegionCode("410000");
        regionMapper.insertRegion(region);
    }

    /**
     * 查询所有地址
     */
    @Test
    public void testQueryRegion(){
        Region region = new Region();
        List<Region> list = regionMapper.selectRegionList(region);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
    }

    /**
     * 根据ID查询单个记录
     */
    @Test
    public void testSelectRegionById(){
        regionMapper.selectRegionById(1L);
    }


}

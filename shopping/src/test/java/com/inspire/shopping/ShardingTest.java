//package com.inspire.shopping;
//
//import com.inspire.shopping.dao.ProductDao;
//import com.inspire.shopping.dao.ProductDescriptDao;
//import com.inspire.shopping.dao.RegionDao;
//import com.inspire.shopping.dao.StoreInfoDao;
//import com.inspire.shopping.entity.Region;
//import com.inspire.shopping.entity.StoreInfo;
//import com.inspire.shopping.entity.ProductInfo;
//import com.inspire.shopping.service.ProductService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * @author Administrator
// * @version 1.0
// **/
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = ShoppingBootstrap.class)
//public class ShardingTest {
//
//    @Autowired
//    ProductService productService;
//
//    @Autowired
//    ProductDao productDao;
//
//    @Autowired
//    private ProductDescriptDao productDescriptDao;
//
//    @Autowired
//    private StoreInfoDao storeInfoDao;
//
//    @Autowired
//    private RegionDao regionDao;
//
//    //添加商品 -->都在一个分区，所有使用本地事务即可
//    @Test
//    public void testCreateProduct(){
//        for (int i=1;i<10;i++){
//            ProductInfo productInfo = new ProductInfo();
//            productInfo.setStoreInfoId(1L);//店铺id
//
//            productInfo.setProductName("薯片"+i);//商品名称
//            productInfo.setSpec("大号");
//            productInfo.setPrice(new BigDecimal(30));
//            productInfo.setRegionCode("110100");
//            productInfo.setDescript("薯片！！！"+i);//商品描述
//            productService.createProduct(productInfo);
//        }
//
//    }
//
//    //查询商品
//    @Test
//    public void testQueryProduct(){
//
//        List<ProductInfo> productInfos = productService.queryProduct(2, 2);
//        System.out.println(productInfos);
//    }
//
//    //统计商品总数
//    @Test
//    public void testSelectCount(){
//
//        int i = productDao.selectCount();
//
//        System.out.println(i);
//    }
//
//    //分组统计商品
//    @Test
//    public void testSelectProductGroupList(){
//
//        List<Map> maps = productDao.selectProductGroupList();
//
//        System.out.println(maps);
//    }
//    //各种查询
//    // TODO: 19/10/9  根据商品Id查询商品详情
//    @Test
//    public void selectProductDescriptbyProductIds(){
//
//        List<Long> productIds = new ArrayList<>();
//        productIds.add(384021504406323200L);
//        productIds.add(384021506042101761L);
//        List<Map> descripts = productDescriptDao.selectProductDescriptbyProductIds(productIds);
//        System.out.println(descripts);
//    }
//
//    // TODO: 19/10/9  根据商品Id查询商品信息+详情信息
//    @Test
//    public void selectProductbyProductIds(){
//
//        List<Long> productIds = new ArrayList<>();
//        productIds.add(384021504406323200L);
//        productIds.add(384021506042101761L);
//        List<Map> products = productDao.selectProductbyProductIds(productIds);
//        System.out.println(products);
//    }
//
//    // TODO: 19/10/9 根据店铺ID查询 查询店铺下的所有商品 ----->查询指定的库
//    @Test
//    public void selectProductByStoreIds(){
//
//        List<Long> storeIds = new ArrayList<>();
//        storeIds.add(1L);
//
//        List<Map> products = productDao.selectProductByStoreIds(storeIds);
//        System.out.println(products);
//    }
//    // TODO: 19/10/9  根据ID查询店铺信息
//    @Test
//    public void selectStoreInfoByStoreIds(){
//
//        List<Long> storeIds = new ArrayList<>();
//        storeIds.add(1L);
//        storeIds.add(2L);
//
//        List<Map> stores = storeInfoDao.selectStoreInfoByStoreIds(storeIds);
//        System.out.println(stores);
//    }
//    // TODO: 19/10/9  查询某个区域下的所有店铺
//    @Test
//    public void selectStoreInfoByCode(){
//
//        List<String> storeIds = new ArrayList<>();
//        storeIds.add("110100");
//
//        List<Map> stores = storeInfoDao.selectStoreInfoByCode(storeIds);
//        System.out.println(stores);
//    }
//
//    //添加数据：
//    // TODO: 19/10/9 添加地区  ---->向多个库主库添加数据
//    @Test
//    public void testInsertRegion(){
//        Region region = new Region();
//        region.setId(5L);
//        region.setRegionCode("410200");
//        region.setRegionName("开封市");
//        region.setLevel(1);
//        region.setParentRegionCode("410000");
//        regionDao.insertRegion(region);
//    }
//
//    // TODO: 19/10/9 添加店铺
//    @Test
//    public void testInsertStoreInfo(){
//        StoreInfo storeInfo = new StoreInfo();
//        storeInfo.setId(3L);
//        storeInfo.setRegionCode("410200");
//        storeInfo.setStoreName("XX服装店");
//        storeInfo.setReputation(5);
//        storeInfoDao.insertStoreInfo(storeInfo);
//    }
//
//    //高级查询：
//    // TODO: 19/10/9 查询最后一条数据 order by limit
//    // TODO: 19/10/9 查询最大的一条数据 max
//    // TODO: 19/10/9 查询每个店铺中的商品种类 group by
//    // TODO: 19/10/9 分页
//
//
//
//
//    //分布式事务
//
//}

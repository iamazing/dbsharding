package com.inspire.shopping;

import com.inspire.shopping.entity.StoreInfo;
import com.inspire.shopping.mapper.StoreInfoMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 * @apiNote store是单独的一个库，存储店铺信息
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShoppingBootstrap.class)
public class StoreInfoTest {

    @Autowired
    private StoreInfoMapper storeInfoMapper;

    //添加数据：
    @Test
    public void testInsertStore(){
        StoreInfo storeInfo = new StoreInfo();
        storeInfo.setRegionCode("410300");
        storeInfo.setStoreName("茶叶店");
        storeInfo.setReputation(5);
        storeInfoMapper.insertStoreInfo(storeInfo);
    }

    /**
     * 查询所有店铺基本信息
     */
    @Test
    public void testQueryStoreInfo(){
        StoreInfo storeInfo = new StoreInfo();
        List<StoreInfo> list = storeInfoMapper.selectStoreInfoList(storeInfo);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
    }


    /**
     * 查询所有店铺--带店铺地址信息
     */
    @Test
    public void testQueryStoreInfoWithRegion(){
        StoreInfo storeInfo = new StoreInfo();
        List<StoreInfo> list = storeInfoMapper.selectStoreInfoWithRegionList(storeInfo);
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i).toString());
        }
    }


    /**
     * 根据ID查询单个店铺
     */
    @Test
    public void testSelectStoreInfoById(){
        StoreInfo storeInfo = storeInfoMapper.selectStoreInfoById(1L);
        System.out.println(storeInfo.toString());
    }

}
